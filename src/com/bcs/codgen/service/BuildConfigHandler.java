package com.bcs.codgen.service;

import java.util.Map;

import com.bcs.codgen.util.ProjectConfig;

public interface BuildConfigHandler {
	/**
	 * 初始化配置时处理
	 * @param buildConfig
	 */
	void initConfig(BuildConfig buildConfig);
	/**
	 * 获取数据模型前处理
	 * @param buildConfig
	 */
	void beforeGetDataModel(BuildConfig buildConfig);
	
	/**
	 * 获取数据模型后处理
	 * @param buildConfig
	 */
	void afterGetDataModel(BuildConfig buildConfig);
	
	/**
	 * 获取输出模型前处理
	 * @param buildConfig
	 */
	void beforeGetOutputModel(BuildConfig buildConfig);
	
	/**
	 * 获取输出模型后处理
	 * @param buildConfig
	 */
	void afterGetOutputModel(BuildConfig buildConfig);
}
